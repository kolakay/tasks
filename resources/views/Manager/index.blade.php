<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
    <style>
        [v-cloak]{
            display: none;
        }
    </style>
</head>
<body>
    <div id="app" v-cloak>
        @csrf
        <div class="row p-5">
            <div class="col-12">
                <a href="#add-project" class="btn btn-primary px-4 py-2 mt-4 mb-4" data-toggle="modal" @click="clearProject">
                    Add Project
                </a>           
            </div>
            <div class="col-4 mt-2" v-for="(project, index) in projects">
                <div class="card ">
                    <div class="card-body">
                      <h5 class="card-title">@{{project.name}}</h5>
                        <a href="#add-task" data-toggle="modal" class="card-link" @click="selectProject(index)">Add Task</a>
                        <a href="#update-project" data-toggle="modal" class="card-link text-dark" @click="selectProject(index)">Update Project</a>
                        <a href="#" class="card-link text-danger" @click="deleteProject(index)">Delete</a>
                    </div>
                </div>
            </div>
    
        </div>
        
        <div class="row p-5" >
            <div class="col-12">
                <h5>View Tasks By Project</h5>
                <select class="form-control" v-model="projectIndex" @change="setProjectTasks">
                    <option value="0" disabled selected>Select Project</option>
                    <option :value="index" v-for="(selectedProject, index) in projects">@{{ selectedProject.name}}</option>
                </select>
            </div>
            <div class="col-12 mt-4" v-if="sortedProject.tasks.length > 0">
                <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Priority</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr v-for="(selectedTask, taskIndex) in sortedProject.tasks">
                        <th scope="row">@{{ selectedTask.name}}</th>
                        <td>@{{ selectedTask.priority}}</td>
                        <td>@{{ selectedTask.created_at}}</td>
                        <td>
                            <a href="#update-task" data-toggle="modal" class="card-link text-dark" @click="selectTask(selectedTask)">Update Project</a>
                            <a href="#" class="card-link text-danger" @click="deleteTask(selectedTask, taskIndex)">Delete</a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            </div>
        </div>
        <div class="modal fade" id="add-project" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content border-0">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
    
                    <div class="modal-body">
                        <div id="createProjectForm" class="px-5 pb-5">
                            <h3 class="font-weight-light mb-4">Add Project</h3>
                            <div class="form-group">
                                <label class="small text-uppercase">Name</label>
                                <input type="text" class="form-control" placeholder="My Project"
                                    v-model="project.name"
                                />
                            </div>
    
                            <div class="mt-4 text-right">
                                <button type="button" class="btn btn-primary px-4 py-2"
                                    form="createProjectForm"
                                    v-on:click="addProject" :disabled="isLoading">
                                    <span v-if="!isLoading">Create</span>
                                    <div class="spinner-border text-light" role="status" v-if="isLoading">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="update-project" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content border-0">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
    
                    <div class="modal-body">
                        <div id="createProjectForm" class="px-5 pb-5">
                            <h3 class="font-weight-light mb-4">Update Project</h3>
                            <div class="form-group">
                                <label class="small text-uppercase">Name</label>
                                <input type="text" class="form-control" placeholder="My Project"
                                    v-model="project.name"
                                />
                            </div>
    
                            <div class="mt-4 text-right">
                                <button type="button" class="btn btn-primary px-4 py-2"
                                    form="createProjectForm"
                                    v-on:click="updateProject" :disabled="isLoading">
                                    <span v-if="!isLoading">Update</span>
                                    <div class="spinner-border text-light" role="status" v-if="isLoading">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="add-task" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content border-0">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
    
                    <div class="modal-body">
                        <div id="createProjectForm" class="px-5 pb-5">
                            <h3 class="font-weight-light mb-4">Add Task</h3>
                            <div class="form-group">
                                <label class="small text-uppercase">Name</label>
                                <input type="text" class="form-control" placeholder="My Project"
                                    v-model="task.name"
                                />
                            </div>

                            <div class="form-group">
                                <label class="small text-uppercase">Priority</label>
                                <select class="form-control" v-model="task.priority">
                                    <option value="highest">Highest</option>
                                    <option value="high">High</option>
                                    <option value="low">Low</option>
                                    <option value="lowest">Lowest</option>
                                </select>
                            </div>
    
                            <div class="mt-4 text-right">
                                <button type="button" class="btn btn-primary px-4 py-2"
                                    form="createProjectForm"
                                    v-on:click="addTask" :disabled="isLoading">
                                    <span v-if="!isLoading">Create</span>
                                    <div class="spinner-border text-light" role="status" v-if="isLoading">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="update-task" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content border-0">
                    <div class="modal-header border-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
    
                    <div class="modal-body">
                        <div id="createProjectForm" class="px-5 pb-5">
                            <h3 class="font-weight-light mb-4">Update Task</h3>
                            <div class="form-group">
                                <label class="small text-uppercase">Name</label>
                                <input type="text" class="form-control" placeholder="My Project"
                                    v-model="task.name"
                                />
                            </div>

                            <div class="form-group">
                                <label class="small text-uppercase">Priority</label>
                                <select class="form-control" v-model="task.priority">
                                    <option value="highest">Highest</option>
                                    <option value="high">High</option>
                                    <option value="low">Low</option>
                                    <option value="lowest">Lowest</option>
                                </select>
                            </div>
    
                            <div class="mt-4 text-right">
                                <button type="button" class="btn btn-primary px-4 py-2"
                                    form="createProjectForm"
                                    v-on:click="updateTask" :disabled="isLoading">
                                    <span v-if="!isLoading">Create</span>
                                    <div class="spinner-border text-light" role="status" v-if="isLoading">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   
    
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script src="https://unpkg.com/vue"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
    <script src="https://unpkg.com/element-ui/lib/index.js"></script>
    <script>
        new Vue({
            el: "#app",
            data: {
                isLoading : false,
                projectIndex: 0,
                project : {
                    name: '',
                    tasks: []
                },
                sortedProject : {
                    name: '',
                    tasks: []
                },
                projects: JSON.parse(`{!! json_encode($projects)!!}`),
                task: {
                    project_id: '',
                    name : '',
                    priority: 'high',
                },
                tasks: [],
                url: {
                    addProject: `{{ route('projects.add') }}`,
                    updateProject: `{{ route('projects.update') }}`,
                    deleteProject: `{{ route('projects.delete') }}`,
                    addTask: `{{ route('tasks.add') }}`,
                    updateTask: `{{ route('tasks.update') }}`,
                    deleteTask: `{{ route('tasks.delete') }}`,
                },
            },
            methods: {
                addProject() {
                    const formData = new FormData();

                    formData.append('_token', $('input[name=_token]').val());

                    for ( var key in this.project ) {
                        let value = this.project[key];
                        formData.append(key, value);
                    }

                    this.isLoading = true;

                    axios.post(this.url.addProject, formData)
                        .then((response) => {
                            this.isLoading = false;
                            this.projects.push(
                                    Object.assign({}, response.data.project, {})
                            );
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });
                        })
                        .catch((error) => {
                            this.isLoading = false;
                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        })
                },
                updateProject() {
                    const formData = new FormData();

                    formData.append('_token', $('input[name=_token]').val());

                    for ( var key in this.project ) {
                        let value = this.project[key];
                        formData.append(key, value);
                    }

                    this.isLoading = true;

                    axios.post(this.url.updateProject, formData)
                        .then((response) => {
                            this.isLoading = false;
                            var updatedProject = response.data.project;
                            this.projects = this.projects.map((project) => {
                                if (project.id === updatedProject.id) {
                                    project = Object.assign({}, updatedProject);
                                }
                                return project;
                            });
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });
                        })
                        .catch((error) => {
                            this.isLoading = false;
                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        })
                },
                selectProject(index){
                    this.project = Object.assign({}, this.projects[index]);
                    this.task = {
                        project_id: '',
                        name : '',
                        priority: 'high',
                    };
                },
                clearProject(){
                    this.project = {
                        name: '',
                        tasks: []
                    };
                },
                deleteProject(index) {
                        let selectedProject = Object.assign({}, this.projects[index]);
                        selectedProject._token = $('input[name=_token]').val();

                        const customAlert = swal({
                            title: 'Warning',
                            text: `Are you sure you want to delete this Project? This action cannot be undone.`,
                            icon: 'warning',
                            closeOnClickOutside: false,
                            buttons: {
                                cancel: {
                                    text: "cancel",
                                    visible: true,
                                    className: "",
                                    closeModal: true,
                                },
                                confirm: {
                                    text: "Confirm",
                                    value: 'delete',
                                    visible: true,
                                    className: "btn-danger",
                                }
                            }
                        });
                        customAlert.then(value => {
                            if (value == 'delete') {
                                this.isLoading = true;
                                axios.delete(this.url.deleteProject, {data: selectedProject})
                                    .then(response => {
                                        this.isLoading = false;
                                        this.projects.splice(index, 1);
                                        this.$notify({
                                            title: 'Success',
                                            message: response.data.message,
                                            type: 'success'
                                        });

                                    }).catch(error => {
                                        if (error.response) {
                                            this.isLoading = false;
                                            this.$notify.error({
                                                title: 'Error',
                                                message: error.response.data.message
                                            });
                                        }
                                    });

                            }
                        });
                },
                setProjectTasks(){
                    this.sortedProject = Object.assign({}, this.projects[this.projectIndex]);
                },
                addTask() {
                    const formData = new FormData();

                    formData.append('_token', $('input[name=_token]').val());
                    this.task.project_id = this.project.id;
                    for ( var key in this.task ) {
                        let value = this.task[key];
                        formData.append(key, value);
                    }

                    this.isLoading = true;

                    axios.post(this.url.addTask, formData)
                        .then((response) => {
                            this.isLoading = false;
                            this.project.tasks.push(
                                    Object.assign({}, response.data.task, {})
                            );
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });
                        })
                        .catch((error) => {
                            this.isLoading = false;
                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        })
                },

                updateTask() {
                    const formData = new FormData();

                    formData.append('_token', $('input[name=_token]').val());
                    this.task.project_id = this.project.id;
                    for ( var key in this.task ) {
                        let value = this.task[key];
                        formData.append(key, value);
                    }

                    this.isLoading = true;

                    axios.post(this.url.updateTask, formData)
                        .then((response) => {
                            this.isLoading = false;
                            var updatedTask = response.data.task;
                            this.sortedProject.tasks.map((task) => {
                                if (task.id === updatedTask.id) {
                                    task = Object.assign({}, updatedTask);
                                }
                                return task;
                            });
                            this.updateProjects();
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });
                        })
                        .catch((error) => {
                            this.isLoading = false;
                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        })
                },

                selectTask(selectedTask){
                    this.task = selectedTask;
                },
                deleteTask(selectedTask, index){
                    selectedTask._token = $('input[name=_token]').val();

                    const customAlert = swal({
                        title: 'Warning',
                        text: `Are you sure you want to delete this Task? This action cannot be undone.`,
                        icon: 'warning',
                        closeOnClickOutside: false,
                        buttons: {
                            cancel: {
                                text: "cancel",
                                visible: true,
                                className: "",
                                closeModal: true,
                            },
                            confirm: {
                                text: "Confirm",
                                value: 'delete',
                                visible: true,
                                className: "btn-danger",
                            }
                        }
                    });
                    customAlert.then(value => {
                        if (value == 'delete') {
                            this.isLoading = true;
                            axios.delete(this.url.deleteTask, {data: selectedTask})
                                .then(response => {
                                    this.isLoading = false;
                                    this.sortedProject.tasks.splice(index, 1);
                                    this.updateProjects();
                                    this.$notify({
                                        title: 'Success',
                                        message: response.data.message,
                                        type: 'success'
                                    });

                                }).catch(error => {
                                    if (error.response) {
                                        this.isLoading = false;
                                        this.$notify.error({
                                            title: 'Error',
                                            message: error.response.data.message
                                        });
                                    }
                                });

                        }
                    });
                },
                updateProjects(){
                    this.projects = this.projects.map((project) => {
                        if (project.id === this.sortedProject.id) {
                            project = Object.assign({}, this.sortedProject);
                        }
                        return project;
                    });
                }
            }
        })
    </script>
</body>
</html>