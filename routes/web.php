<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'index'])->name('index');

Route::post('/projects/add', [DashboardController::class, 'addProject'])->name('projects.add');
Route::post('/projects/update', [DashboardController::class, 'updateProject'])->name('projects.update');
Route::delete('/projects/delete', [DashboardController::class, 'deleteProject'])->name('projects.delete');

Route::post('/tasks/add', [DashboardController::class, 'addTask'])->name('tasks.add');
Route::post('/tasks/update', [DashboardController::class, 'updateTask'])->name('tasks.update');
Route::delete('/tasks/delete', [DashboardController::class, 'deleteTask'])->name('tasks.delete');
//Route::get('/', [DashboardController::class, 'index'])->name('index');

