<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectModel extends Model
{
    use HasFactory;

    protected $table ="projects";
    protected $with = [
        'tasks'
    ];

    /**
     * Get the comments for the blog post.
     */
    public function tasks()
    {
        return $this->hasMany(TaskModel::class, 'project_id', 'id');
    }
}
