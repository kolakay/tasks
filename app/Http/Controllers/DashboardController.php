<?php

namespace App\Http\Controllers;

use Log;
use App\Models\ProjectModel;
use App\Models\TaskModel;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        try {
            $projects = ProjectModel::all();
            $data = [
                'projects' => $projects
            ];

            return view('Manager.index', $data);
        } catch (\Exception $error) {
            abort(503);
        }
    }

    public function addProject(Request $request)
    {
        try {
            

            $project = new ProjectModel();
            $project->name = $request->name;
            $project->save();

            return response()->json([
                'error' => false,
                'project' => $project,
                'message' => "project was added successfully"
            ], 200);
        } catch (Exception $error) {
            Log::info('DashboardController@add error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }

    public function updateProject(Request $request)
    {
        try {
            

            $project = ProjectModel::where('id', $request->id)->first();
            if(!$project){
                return response()->json([
                    'error' => true,
                    'message' => "Project not found",
                    'messages' => [],
                ], 404);
            }
            $project->name = $request->name;
            $project->save();

            return response()->json([
                'error' => false,
                'project' => $project,
                'message' => "project was updated successfully"
            ], 200);
        } catch (Exception $error) {
            Log::info('DashboardController@update error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }

    public function deleteProject(Request $request)
    {
        try {
            $project = ProjectModel::where('id', $request->id)->first();
            if(!$project){
                return response()->json([
                    'error' => true,
                    'message' => "Project not found",
                    'messages' => [],
                ], 404);
            }

            TaskModel::where('project_id', $project->id)->delete();
            $project->delete();

            return response()->json([
                'error' => false,
                'project' => $project,
                'message' => "project was deleted successfully"
            ], 200);
        } catch (Exception $error) {
            Log::info('DashboardController@delete error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }

    public function addTask(Request $request)
    {
        try {
            
            $project = ProjectModel::where('id', $request->project_id)->first();
            if(!$project){
                return response()->json([
                    'error' => true,
                    'message' => "Project not found",
                    'messages' => [],
                ], 404);
            }
            $task = new TaskModel();
            $task->name = $request->name;
            $task->priority = $request->priority;
            $task->project_id = $project->id;
            $task->save();

            return response()->json([
                'error' => false,
                'task' => $task,
                'message' => "Task was added successfully"
            ], 200);
        } catch (Exception $error) {
            Log::info('DashboardController@addTask error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }

    public function updateTask(Request $request)
    {
        try {
            
            $task = TaskModel::where('id', $request->id)->first();
            if(!$task){
                return response()->json([
                    'error' => true,
                    'message' => "Task not found",
                    'messages' => [],
                ], 404);
            }
            $task->name = $request->name;
            $task->priority = $request->priority;
            $task->save();

            return response()->json([
                'error' => false,
                'task' => $task,
                'message' => "Task was updated successfully"
            ], 200);
        } catch (Exception $error) {
            Log::info('DashboardController@updateTask error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }

    public function deleteTask(Request $request)
    {
        try {
            $task = TaskModel::where('id', $request->id)->first();
            if(!$task){
                return response()->json([
                    'error' => true,
                    'message' => "Task not found",
                    'messages' => [],
                ], 404);
            }

            $task->delete();

            return response()->json([
                'error' => false,
                'message' => "task was deleted successfully"
            ], 200);
        } catch (Exception $error) {
            Log::info('DashboardController@deleteTask error message: ' . $error->getMessage());
            $message = "Unable to complete request.";
            return response()->json(['message' => $message], 500);
        }
    }
}
